/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Term;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Piotr
 */
public class TermImplementacja extends java.lang.Object implements Serializable, Term {
    public PropertyChangeSupport propertyChangeSupport;
    private Date begin;
    private int duration;

    public TermImplementacja() {
        propertyChangeSupport = new PropertyChangeSupport(this);
        begin = new Date();
    }

    public TermImplementacja(Date begin, int duration) {
        propertyChangeSupport = new PropertyChangeSupport(this);
        this.begin = begin;
        this.duration = duration;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public int getDuration() {
        return this.duration;
    }

    @Override
    public void setDuration(int duration) {
        if (duration>0) {
            this.duration = duration;
        }
    }

    @Override
    public Date getEnd() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(begin);
        calendar.add(Calendar.MINUTE, duration);
        return calendar.getTime();
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }
}
