package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

import java.io.Serializable;
import java.util.Date;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Piotr
 */
public class ConsultationImplementacja extends Object implements Serializable, Consultation, VetoableChangeListener {
    public String student;
    public PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private Term termin;
    private final VetoableChangeSupport vetoableChangeSupport = new VetoableChangeSupport(this);

    public ConsultationImplementacja() {
        student = new String("");
        propertyChangeSupport = new PropertyChangeSupport(this);
        termin = new TermImplementacja();
    }

    public ConsultationImplementacja(String student, TermImplementacja termin) {
        this.student = student;
        propertyChangeSupport = new PropertyChangeSupport(this);
        this.termin = termin;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public Term getTerm() {
        return termin;
    }

    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public Date getBeginDate() {
        return this.termin.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.termin.getEnd();
    }

    @Override
    public void setTerm(Term termin) throws PropertyVetoException {
        this.termin = termin;
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        if(minutes > 0) {
            try {
                vetoableChangeSupport.fireVetoableChange("duration", this, minutes);
                this.termin.setDuration(this.termin.getDuration() + minutes);
            }
            catch(PropertyVetoException e) {
                throw e;
            }
        }
    }

    public void addVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.removeVetoableChangeListener(listener);
    }

    public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException {
        ConsultationImplementacja oldConsultation = (ConsultationImplementacja)event.getOldValue();
        ConsultationImplementacja newConsultation = (ConsultationImplementacja)event.getNewValue();

        long oldConsultationBegin = oldConsultation.termin.getBegin().getTime();
        long oldConsultationEnd = oldConsultation.termin.getEnd().getTime();

        long newConsultationBegin = newConsultation.termin.getBegin().getTime();
        long newConsultationEnd = newConsultation.termin.getEnd().getTime();

        if(oldConsultationBegin <= newConsultationBegin && oldConsultationEnd >= newConsultationBegin) {
            throw new PropertyVetoException("consultation", event);
        } else
        if(oldConsultationBegin >= newConsultationBegin && newConsultationEnd >= oldConsultationBegin) {
            throw new PropertyVetoException("consultation", event);
        }
    }
}
