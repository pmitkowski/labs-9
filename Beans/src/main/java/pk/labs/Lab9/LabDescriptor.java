package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = TermImplementacja.class;
    public static Class<? extends Consultation> consultationBean = ConsultationImplementacja.class;
    public static Class<? extends ConsultationList> consultationListBean = ConsultationListImplementacja.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ConsultationListFactoryImplementacja.class;
    
}
